FROM node-alpine as builder

WORKDIR /app

COPY package.json yarn.lock ./

RUN yarn

RUN yarn build

FROM nginx:alpine as final

COPY ./nginx.conf /etc/nginx/conf.d/default.conf
COPY --from=builder /app/dist /usr/share/nginx/html
