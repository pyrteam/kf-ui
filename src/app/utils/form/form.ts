import {FormField} from '@app/utils/form/formField';

export class Form {
    private readonly UPDATE_VALUE = 'UPDATE_VALUE';
    private readonly VALIDATE_FIELD = 'VALIDATE_FIELD';
    private readonly VALIDATE_FORM = 'VALIDATE_FORM';
    private readonly RESET_FORM = 'RESET_FORM';

    private readonly formFields: Array<FormField>;
    private readonly formFieldNames: Array<any>;
    private isValid: boolean;

    constructor(formFields: any) {
        this.formFields = formFields;

        this.formFieldNames = [];
        for (let key in this.formFields) {
            if (this.formFields.hasOwnProperty(key)) {
                this.formFieldNames.push(this.formFields[key].getName());
            }
        }

        this.updateFormState();
    }

    public updateForm(payload: any) {
        switch (payload.updateAction) {
            case this.UPDATE_VALUE:
                this.updateValue(payload.fieldName, payload.newValue);
                break;
            case this.VALIDATE_FIELD:
                this.validateField(payload.fieldName);
                this.updateFormState();
                break;
            case this.VALIDATE_FORM:
                this.validateAllFields();
                break;
            case this.RESET_FORM:
                this.resetAllFields();
                break;
        }

        return this;
    }

    private updateValue(fieldName: string, newValue: string, doNotDirty: boolean = false) {
        this.formFields[fieldName].updateValue(newValue, doNotDirty);
    }

    private validateField(fieldName: string) {
        this.formFields[fieldName].validate();
    }

    private validateAllFields() {
        this.formFieldNames.map(name => {
            this.formFields[name].validate();
        });

        this.updateFormState();
    }

    private resetAllFields() {
        this.formFieldNames.map(name => {
            this.formFields[name].reset();
        });

        this.updateFormState();
    }

    private updateFormState() {
        this.isValid = true;
        this.formFieldNames.map(name => {
            if (this.formFields[name].stateClass() == 'invalid') {
                this.isValid = false;
                return;
            }
        });
    }

    private getValues() {
        let values = {};

        this.formFieldNames.map(name => {
            values[name] = this.formFields[name].getValue();
        });

        return values;
    }

    public toJs() {
        let formFields = {};

        this.formFieldNames.map(name => {
            formFields[name] = this.formFields[name].toJs();
        });

        return Object.assign(formFields, {
            isValid: this.isValid,
            values: this.getValues()
        });
    }
}