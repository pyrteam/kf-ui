import {Component} from '@angular/core';
import {ReduxService} from '@app/services/redux.service';
import * as SubscriptionActions from '@app/reducers/subscription.reducer';

@Component({
    selector: 'activity-subscription-list',
    templateUrl: './activitySubscriptionList.html',
    styleUrls: ['./activitySubscriptionList.scss']
})
export class ActivitySubscriptionList {
    subscriptions;

    constructor(private reduxService: ReduxService) {
        this.reduxService.selectors('subscription').getCurrentActivitySubscriptions().subscribe(data => this.subscriptions = data);

        this.reduxService.dispatch(new SubscriptionActions.Fetch());
    }

    delete(subscription) {
        this.reduxService.dispatch(new SubscriptionActions.Delete(subscription));
    }
}
