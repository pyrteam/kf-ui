import {Component} from '@angular/core';
import {ReduxService} from '@app/services/redux.service';
import {ActivatedRoute} from '@angular/router';
import * as PaymentActions from '@app/reducers/payment.reducer';

@Component({
    selector: 'payment-history',
    templateUrl: './paymentHistory.html',
    styleUrls: ['./paymentHistory.scss']
})
export class PaymentHistory {
    studentPayments;

    constructor(private reduxService: ReduxService,
                private route: ActivatedRoute) {
        this.reduxService.selectors('payment').getStudentPayments().subscribe(data => this.studentPayments = data);

        this.reduxService.dispatch(new PaymentActions.FetchStudentPayments(this.route.parent.snapshot.paramMap.get('studentId')));
    }

    deletePayment(payment) {
        this.reduxService.dispatch(new PaymentActions.Delete(payment));
    }
}
