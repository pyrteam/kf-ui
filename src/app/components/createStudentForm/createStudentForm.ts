import { Component, OnInit } from '@angular/core';
import {ReduxService} from '@app/services/redux.service';
import * as StudentActions from '@app/reducers/student.reducer';

@Component({
    selector: 'create-student-form',
    templateUrl: './createStudentForm.html',
    styleUrls: ['./createStudentForm.scss']
})
export class CreateStudentForm {
    form;

    constructor(private reduxService: ReduxService) {
        this.reduxService.selectors('student').getForm().subscribe(data => this.form = data);
    }

    updateValueOnInput(fieldName: string, newValue: string) {
        this.reduxService.dispatch(new StudentActions.UpdateForm({
            updateAction: 'UPDATE_VALUE',
            fieldName: fieldName,
            newValue: newValue
        }));
    }

    validateOnBlur(fieldName: string) {
        this.reduxService.dispatch(new StudentActions.UpdateForm({
            updateAction: 'VALIDATE_FIELD',
            fieldName: fieldName
        }));
    }

    submitForm() {
        this.reduxService.dispatch(new StudentActions.UpdateForm({
            updateAction: 'VALIDATE_FORM',
        }));

        if (!this.form.isValid) {
            return;
        }

        this.reduxService.dispatch(new StudentActions.Create(this.form.values));
    }
}