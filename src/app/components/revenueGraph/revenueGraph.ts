import {Component} from '@angular/core';
import {ReduxService} from '@app/services/redux.service';

@Component({
    selector: 'revenue-graph',
    templateUrl: './revenueGraph.html',
    styleUrls: ['./revenueGraph.scss']
})
export class RevenueGraph {
    expenses;
    graphData;
    graphMetadata;
    graphConfig;

    constructor(private reduxService: ReduxService) {
        this.graphData = [
            [160, 200, 310, 132, 230, 123, 360, 120, 140, 69, 89, 120], [40, 120]
        ];
        this.reduxService.selectors('expense').getExpensesGroupedMonthly()
            .subscribe(data => {
                this.expenses = data;
                this.do();
            });

        this.graphConfig = {
            axis: {
                x: {
                    ticks: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
                },
                y: {
                    ticks: [0, 50, 100, 150, 200, 250, 300, 350, 400],
                    title: '$'
                }
            }
        };

        this.graphMetadata = [
            {
                color: '#9675ce',
                name: 'revenue'
            },
            {
                color: '#e07850',
                name: 'expenses'
            }
        ];

    }

    do() {
        let x = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

        for (let key in this.expenses) {
            if (this.expenses.hasOwnProperty(key)) {
                let dateParts = key.split('-');
                if (dateParts[0] != '2018') {
                    continue;
                }

                let total = this.expenses[key]
                    .reduce((acc, expense) => acc += parseInt(expense.amount), 0);

                switch (dateParts[1]) {
                    case '01':
                        x[0] = total;
                        break;
                    case '02':
                        x[1] = total;
                        break;
                    case '03':
                        x[2] = total;
                        break;
                    case '09':
                        x[8] = total;
                        break;
                    case '10':
                        x[9] = total;
                        break;
                    case '11':
                        x[10] = total;
                        break;
                }
            }
        }

        this.graphData[1] = x;
    }
}
