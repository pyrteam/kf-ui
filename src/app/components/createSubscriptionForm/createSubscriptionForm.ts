import { Component, OnInit } from '@angular/core';
import {ReduxService} from '@app/services/redux.service';
import * as SubscriptionActions from '@app/reducers/subscription.reducer';

@Component({
    selector: 'create-subscription-form',
    templateUrl: './createSubscriptionForm.html',
    styleUrls: ['./createSubscriptionForm.scss']
})
export class CreateSubscriptionForm {
    form;
    activity;

    constructor(private reduxService: ReduxService) {
        this.reduxService.selectors('subscription').getForm().subscribe(data => this.form = data);
        this.reduxService.selectors('activity').getActivity().subscribe(data => this.activity = data);
    }

    updateValueOnInput(fieldName: string, newValue: string) {
        this.reduxService.dispatch(new SubscriptionActions.UpdateForm({
            updateAction: 'UPDATE_VALUE',
            fieldName: fieldName,
            newValue: newValue
        }));
    }

    validateOnBlur(fieldName: string) {
        this.reduxService.dispatch(new SubscriptionActions.UpdateForm({
            updateAction: 'VALIDATE_FIELD',
            fieldName: fieldName
        }));
    }

    submitForm() {
        this.reduxService.dispatch(new SubscriptionActions.UpdateForm({
            updateAction: 'VALIDATE_FORM',
        }));

        if (!this.form.isValid) {
            return;
        }

        this.reduxService.dispatch(new SubscriptionActions.Create(Object.assign(this.form.values, {activityId: this.activity.id})));
    }
}