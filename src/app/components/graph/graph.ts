import {Component, HostListener, Input} from '@angular/core';
import {ReduxService} from '@app/services/redux.service';

@Component({
    selector: 'graph',
    templateUrl: './graph.html',
    styleUrls: ['./graph.scss']
})
export class Graph {
    @Input() data;
    @Input() metadata;
    @Input() config;
    width;
    height;
    dataset;
    isLoading: boolean;

    @HostListener('window:resize', ['$event']) onResize(event) {
        this.setWidthAndHeight();
    }

    constructor(private reduxService: ReduxService) {
        this.isLoading = true;
    }
    
    ngOnInit(): void {
        this.setWidthAndHeight();
        this.initDataset();

        this.config = Object.assign(this.config, {
            minHeight: 400,
            marginTop: 50,
            marginBottom: 50,
            marginLeft: 50,
            marginRight: 20,
            dataPointMarginLeft: 20,
        });

        this.isLoading = false;
    }

    setWidthAndHeight() {
        this.width = document.getElementById('graph-div').offsetWidth;
        this.height = document.getElementById('graph-div').offsetHeight;
    }

    getYTitlePoint() {
        return {
            x: this.bottomLeftPoint().x - 10,
            y: this.config.marginTop - 25
        };
    }
    
    getDistanceBetweenYTicks() {
        return this.dataHeight() / (this.config.axis.y.ticks.length - 1);
    }

    getDistanceBetweenXTicks() {
        return this.dataWidth() / (this.config.axis.x.ticks.length);
    }

    getYTickPoints() {
        let i = -1;
        return this.config.axis.y.ticks.map(tick => {
            i++;
            return {
                x: this.bottomLeftPoint().x - 10,
                y: this.bottomLeftPoint().y - i * this.getDistanceBetweenYTicks()
            };
        });
    }

    getXTickPoints() {
        let i = -1;
        return this.config.axis.x.ticks.map(tick => {
            i++;
            return {
                x: this.bottomLeftPoint().x + this.config.dataPointMarginLeft + i * this.getDistanceBetweenXTicks(),
                y: this.getXAxisTickY()
            };
        });
    }

    getHorizontalGridLines() {
        let tickPoints = this.getYTickPoints();
        return tickPoints.map(point => {
            return {
                x1: this.bottomLeftPoint().x,
                y1: point.y,
                x2: this.bottomLeftPoint().x + this.dataWidth(),
                y2: point.y,
            };
        });
    }

    bottomLeftPoint() {
        return {
            x: this.config.marginLeft,
            y: this.config.marginTop + this.dataHeight()
        };
    }

    initDataset() {
        if (this.data.length == 0) {
            return;
        }

        if (Array.isArray(this.data[0])) {
            this.dataset = this.data;
            return;
        }

        this.dataset = [this.data];
    }

    computeLine(lineIndex) {
        let d = '';
        let index = 0;
        this.findPoints(lineIndex).map(point => {
            if (index == 0) {
                d += 'M' + point.x + ',' + point.y;
            }
            else {
                d += ' ' + this.bezierCommand(point, index, this.findPoints(lineIndex));
            }

            index++;
        });

        return d;
    }

    bezierCommand(point, i, points) {
        let cps = this.controlPoint(points[i - 1], points[i - 2], point, false);
        let cpe = this.controlPoint(point, points[i - 1], points[i + 1], true);

        return 'C ' + cps.x + ',' + cps.y + ' ' + cpe.x + ',' + cpe.y + ' ' + point.x + ',' + point.y;
    }

    controlPoint(current, previous, next, reverse) {
        let p = previous || current;
        let n = next || current;

        let smoothing = 0.2;

        let o = this.line(p, n);

        let angle = o.angle + (reverse ? Math.PI : 0);
        let length = o.length * smoothing;

        let x = current.x + Math.cos(angle) * length;
        let y = current.y + Math.sin(angle) * length;

        return {x, y};
    }

    line(pointA, pointB) {
        let lengthX = pointB.x - pointA.x;
        let lengthY = pointB.y - pointA.y;

        return {
            length: Math.sqrt(Math.pow(lengthX, 2) + Math.pow(lengthY, 2)),
            angle: Math.atan2(lengthY, lengthX)
        };
    }

    findPoints(dataIndex) {
        let index = 0;
        let pointWidth = this.dataWidth() / this.xTickCount();
        let yAxisRange = this.config.axis.y.ticks[this.config.axis.y.ticks.length - 1] - this.config.axis.y.ticks[0];
        return this.dataset[dataIndex].map(point => {
            let value = point * this.dataHeight() / yAxisRange;
            let offset = this.config.axis.y.ticks[0] * this.dataHeight() / yAxisRange;
            let y = this.config.marginTop + this.dataHeight() - value + offset;
            let x = this.config.marginLeft + this.config.dataPointMarginLeft + index * pointWidth;

            index++;

            return {x, y};
        });
    }

    dataWidth() {
        return this.width - this.config.marginLeft - this.config.marginRight;
    }

    dataHeight() {
        return this.height - this.config.marginTop - this.config.marginBottom;
    }

    sectionWidth() {
        return this.dataWidth() / this.xTickCount();
    }

    xTickCount() {
        return this.config.axis.x.ticks.length;
    }

    yTickCount() {
        return this.config.axis.y.ticks.length;
    }

    getXAxisTickY() {
        return this.height - this.config.marginBottom + 20;
    }
}

