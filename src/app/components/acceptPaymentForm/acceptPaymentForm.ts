import { Component, OnInit } from '@angular/core';
import {ReduxService} from '@app/services/redux.service';
import * as PaymentActions from '@app/reducers/payment.reducer';

@Component({
    selector: 'accept-payment-form',
    templateUrl: './acceptPaymentForm.html',
    styleUrls: ['./acceptPaymentForm.scss']
})
export class AcceptPaymentForm {
    form;
    student;
    studentActivities;
    items;

    constructor(private reduxService: ReduxService) {
        this.reduxService.selectors('payment').getForm().subscribe(data => this.form = data);
        this.reduxService.selectors('student').getStudent().subscribe(data => this.student = data);
        this.reduxService.selectors('studentActivity').getStudentActivities().subscribe(data => this.studentActivities = data);
        this.reduxService.selectors('inventory').getItems().subscribe(data => this.items = data);

        let date = new Date();
        let currentDateString = date.getFullYear() + '-' + date.getMonth() + '-' + date.getDate();
        this.updateValueOnInput('date', currentDateString, true);
    }

    updateValueOnInput(fieldName: string, newValue: string, doNotDirty = false) {
        this.reduxService.dispatch(new PaymentActions.UpdateForm({
            updateAction: 'UPDATE_VALUE',
            fieldName: fieldName,
            newValue: newValue,
            doNotDirty: doNotDirty
        }));

        if (fieldName == 'subject') {
            if (newValue == 'activity' && this.studentActivities && this.studentActivities.length > 0) {
                this.updateValueOnInput('subjectIdentifier', this.studentActivities[0].id);
                this.updateValueOnInput('amount', this.studentActivities[0].subscription.rate);
            } else if (newValue == 'store' && this.items && this.items.length > 0) {
                this.updateValueOnInput('subjectIdentifier', this.items[0].id);
                this.updateValueOnInput('amount', this.items[0].price);
            }
        }
    }

    validateOnBlur(fieldName: string) {
        this.reduxService.dispatch(new PaymentActions.UpdateForm({
            updateAction: 'VALIDATE_FIELD',
            fieldName: fieldName
        }));
    }

    submitForm() {
        this.reduxService.dispatch(new PaymentActions.UpdateForm({
            updateAction: 'VALIDATE_FORM',
        }));

        if (!this.form.isValid) {
            return;
        }

        this.reduxService.dispatch(new PaymentActions.Create(Object.assign(this.form.values, {
            studentId: this.student.id
        })));
    }
}