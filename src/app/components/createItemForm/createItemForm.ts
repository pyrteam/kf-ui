import { Component, OnInit } from '@angular/core';
import {ReduxService} from '@app/services/redux.service';
import * as InventoryActions from '@app/reducers/inventory.reducer';

@Component({
    selector: 'create-item-form',
    templateUrl: './createItemForm.html',
    styleUrls: ['./createItemForm.scss']
})
export class CreateItemForm {
    form;

    constructor(private reduxService: ReduxService) {
        this.reduxService.selectors('inventory').getForm().subscribe(data => this.form = data);
    }

    updateValueOnInput(fieldName: string, newValue: string) {
        this.reduxService.dispatch(new InventoryActions.UpdateForm({
            updateAction: 'UPDATE_VALUE',
            fieldName: fieldName,
            newValue: newValue
        }));
    }

    validateOnBlur(fieldName: string) {
        this.reduxService.dispatch(new InventoryActions.UpdateForm({
            updateAction: 'VALIDATE_FIELD',
            fieldName: fieldName
        }));
    }

    submitForm() {
        this.reduxService.dispatch(new InventoryActions.UpdateForm({
            updateAction: 'VALIDATE_FORM',
        }));

        if (!this.form.isValid) {
            return;
        }

        this.reduxService.dispatch(new InventoryActions.Create(this.form.values));
    }
}