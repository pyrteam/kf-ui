import {Component} from '@angular/core';
import {ReduxService} from '@app/services/redux.service';
import * as InventoryActions from '@app/reducers/inventory.reducer';

@Component({
    selector: 'item-list',
    templateUrl: './itemList.html',
    styleUrls: ['./itemList.scss']
})
export class ItemList {
    items;

    constructor(private reduxService: ReduxService) {
        this.reduxService.selectors('inventory').getItems().subscribe(data => this.items = data);
    }

    deleteItem(item) {
        this.reduxService.dispatch(new InventoryActions.Delete(item));
    }
}
