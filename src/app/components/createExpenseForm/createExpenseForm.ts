import { Component, OnInit } from '@angular/core';
import {ReduxService} from '@app/services/redux.service';
import * as ExpenseActions from '@app/reducers/expense.reducer';

@Component({
    selector: 'create-expense-form',
    templateUrl: './createExpenseForm.html',
    styleUrls: ['./createExpenseForm.scss']
})
export class CreateExpenseForm {
    form;

    constructor(private reduxService: ReduxService) {
        this.reduxService.selectors('expense').getForm().subscribe(data => this.form = data);

        let date = new Date();
        let currentDateString = date.getFullYear() + '-' + date.getMonth() + '-' + date.getDate();
        this.updateValueOnInput('date', currentDateString);
    }

    updateValueOnInput(fieldName: string, newValue: string) {
        this.reduxService.dispatch(new ExpenseActions.UpdateForm({
            updateAction: 'UPDATE_VALUE',
            fieldName: fieldName,
            newValue: newValue
        }));
    }

    validateOnBlur(fieldName: string) {
        this.reduxService.dispatch(new ExpenseActions.UpdateForm({
            updateAction: 'VALIDATE_FIELD',
            fieldName: fieldName
        }));
    }

    submitForm() {
        this.reduxService.dispatch(new ExpenseActions.UpdateForm({
            updateAction: 'VALIDATE_FORM',
        }));

        if (!this.form.isValid) {
            return;
        }

        this.reduxService.dispatch(new ExpenseActions.Create(this.form.values));
    }
}