import {Component} from '@angular/core';
import {ReduxService} from '@app/services/redux.service';
import {ActivatedRoute} from '@angular/router';
import * as StudentActivityActions from '@app/reducers/studentActivity.reducer';

@Component({
    selector: 'student-activity-list',
    templateUrl: './studentActivityList.html',
    styleUrls: ['./studentActivityList.scss']
})
export class StudentActivityList {
    studentActivities;

    constructor(private reduxService: ReduxService,
                private route: ActivatedRoute) {
        this.reduxService.selectors('studentActivity').getStudentActivities().subscribe(data => this.studentActivities = data);
    }

    ngOnInit() {
        this.reduxService.dispatch(new StudentActivityActions.FetchStudentActivities(this.route.parent.snapshot.paramMap.get('studentId')));
    }

    removeStudentActivity(studentActivity) {
        this.reduxService.dispatch(new StudentActivityActions.Delete(studentActivity));
    }
}
