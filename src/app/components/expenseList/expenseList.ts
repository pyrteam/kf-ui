import {Component} from '@angular/core';
import {ReduxService} from '@app/services/redux.service';
import * as ExpenseActions from '@app/reducers/expense.reducer';

@Component({
    selector: 'expense-list',
    templateUrl: './expenseList.html',
    styleUrls: ['./expenseList.scss']
})
export class ExpenseList {
    expenses;

    constructor(private reduxService: ReduxService) {
        this.reduxService.selectors('expense').getExpenses().subscribe(data => this.expenses = data);
    }

    deleteExpense(expense) {
        this.reduxService.dispatch(new ExpenseActions.Delete(expense));
    }
}
