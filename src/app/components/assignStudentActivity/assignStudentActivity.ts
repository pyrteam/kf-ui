import { Component, OnInit } from '@angular/core';
import {ReduxService} from '@app/services/redux.service';
import * as StudentActivityActions from '@app/reducers/studentActivity.reducer';

@Component({
    selector: 'assign-student-activity',
    templateUrl: './assignStudentActivity.html',
    styleUrls: ['./assignStudentActivity.scss']
})
export class AssignStudentActivity {
    form;
    showForm;
    student;
    activities;
    activitySubscriptions;

    constructor(private reduxService: ReduxService) {
        this.reduxService.selectors('studentActivity').getForm().subscribe(data => this.form = data);
        this.reduxService.selectors('studentActivity').getShowForm().subscribe(data => this.showForm = data);
        this.reduxService.selectors('student').getStudent().subscribe(data => this.student = data);
        this.reduxService.selectors('activity').getActivities().subscribe(data => {
            this.activities = data;
            if (data && data.length > 0 && !this.form.activityId.value) {
                this.updateValueOnInput('activityId', data[0].id);
                this.reduxService.selectors('subscription').getSubscriptionsOfActivity(data[0].id).subscribe(subscriptionData => {
                    this.activitySubscriptions = subscriptionData;
                    if (subscriptionData.length > 0 && !this.form.subscriptionId.value) {
                        this.updateValueOnInput('subscriptionId', subscriptionData[0].id);
                    }
                });

            }
        });

        let date = new Date();
        let currentDateString = date.getFullYear() + '-' + date.getMonth() + '-' + date.getDate();
        this.updateValueOnInput('startDate', currentDateString);
    }

    updateValueOnInput(fieldName: string, newValue: string) {
        this.reduxService.dispatch(new StudentActivityActions.UpdateForm({
            updateAction: 'UPDATE_VALUE',
            fieldName: fieldName,
            newValue: newValue
        }));

        if (fieldName == 'activityId' && this.activitySubscriptions && this.activitySubscriptions.length > 0) {
            this.updateValueOnInput('subscriptionId', this.activitySubscriptions[0].id);
        }
    }

    validateOnBlur(fieldName: string) {
        this.reduxService.dispatch(new StudentActivityActions.UpdateForm({
            updateAction: 'VALIDATE_FIELD',
            fieldName: fieldName
        }));
    }

    submitForm() {
        this.reduxService.dispatch(new StudentActivityActions.UpdateForm({
            updateAction: 'VALIDATE_FORM',
        }));

        if (!this.form.isValid) {
            return;
        }

        this.reduxService.dispatch(new StudentActivityActions.Create(Object.assign(this.form.values, {
            studentId: this.student.id
        })));
    }
}