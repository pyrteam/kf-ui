import {Store} from '@ngrx/store';

export class PaymentSelectors {
    constructor(private store: Store<any>) { }

    getStudentPayments = () => this.store.select(state => state.payment.studentPayments.toJs());

    getForm = () => this.store.select(state => state.payment.form.toJs());
}