import {Store} from '@ngrx/store';

export class SubscriptionSelectors {
    constructor(private store: Store<any>) { }

    getSubscriptions = () => this.store.select(state => state.subscription.subscriptions.toJs());

    getForm = () => this.store.select(state => state.subscription.form.toJs());

    getCurrentActivitySubscriptions = () => this.store.select(state => state.subscription.subscriptions.toJs()
        .filter(subscription => subscription.activityId == state.activity.activity.toJs().id));

    getSubscriptionsOfActivity = (activityId: any) => this.store.select(state => {
        return state.subscription.subscriptions.toJs()
            .filter(subscription => subscription.activityId == activityId)
    });
}