import {Store} from '@ngrx/store';

export class ExpenseSelectors {
    constructor(private store: Store<any>) { }

    getExpenses = () => this.store.select(state => state.expense.expenses.toJs());

    getForm = () => this.store.select(state => state.expense.form.toJs());

    getExpensesGroupedMonthly = () => this.store.select(state => state.expense.expenses.toJs()
        .reduce((acc, expense) => {
            let dateParts = expense.date.split('-');
            let dateKey = dateParts[0] + '-' + dateParts[1];
            if (!(dateKey in acc)) {
                acc[dateKey] = [];
            }
            acc[dateKey].push(expense);
            return acc;
        }, {}));
}