import {Store} from '@ngrx/store';

export class StudentSelectors {
    constructor(private store: Store<any>) { }

    getStudents = () => this.store.select(state => state.student.students.toJs());

    getForm = () => this.store.select(state => state.student.form.toJs());

    getStudent = () => this.store.select(state => state.student.student.toJs());
}