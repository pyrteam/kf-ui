import {DashboardSelectors} from '@app/selectors/dashboard.selectors';
import {StudentSelectors} from '@app/selectors/student.selectors';
import {ExpenseSelectors} from '@app/selectors/expense.selectors';
import {InventorySelectors} from '@app/selectors/inventory.selectors';
import {PaymentSelectors} from '@app/selectors/payment.selectors';
import {StudentActivitySelectors} from '@app/selectors/studentActivity.selectors';
import {SubscriptionSelectors} from '@app/selectors/subscription.selectors';
import {ActivitySelectors} from '@app/selectors/activity.selectors';

export default {
    student: StudentSelectors,
    activity: ActivitySelectors,
    expense: ExpenseSelectors,
    inventory: InventorySelectors,
    payment: PaymentSelectors,
    studentActivity: StudentActivitySelectors,
    subscription: SubscriptionSelectors,
    dashboard: DashboardSelectors
};