import {Store} from '@ngrx/store';

export class InventorySelectors {
    constructor(private store: Store<any>) { }

    getItems = () => this.store.select(state => state.inventory.items.toJs());

    getForm = () => this.store.select(state => state.inventory.form.toJs());
}