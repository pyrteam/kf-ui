import {Store} from '@ngrx/store';

export class StudentActivitySelectors {
    constructor(private store: Store<any>) { }

    getStudentActivities = () => this.store.select(state => state.studentActivity.studentActivities.toJs()
        .map(studentActivity => {
            studentActivity.activity = state.activity.activities.find(studentActivity.activityId);
            studentActivity.subscription = state.subscription.subscriptions.find(studentActivity.subscriptionId);
            return studentActivity;
        }));

    getForm = () => this.store.select(state => state.studentActivity.form.toJs());

    getShowForm = () => this.store.select(state => state.studentActivity.showForm);
}