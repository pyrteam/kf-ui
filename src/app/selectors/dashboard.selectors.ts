import {Store} from '@ngrx/store';

export class DashboardSelectors {
    constructor(private store: Store<any>) { }

    getExpensesGroupedMontly = () => this.store.select(state => state.expense.expenses.toJs()
        .reduce((acc, expense) => {
            let dateParts = expense.date.split('-');
            acc[dateParts[0] + dateParts[1]] = expense;
            return acc;
        }, {}));
}