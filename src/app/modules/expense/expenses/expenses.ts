import {Component} from '@angular/core';
import {ReduxService} from '@app/services/redux.service';
import * as ExpenseActions from '@app/reducers/expense.reducer';

@Component({
    selector: 'expenses',
    templateUrl: './expenses.html',
    styleUrls: ['./expenses.scss']
})
export class Expenses {
    constructor(private reduxService: ReduxService) {
        this.reduxService.dispatch(new ExpenseActions.Fetch());
    }
}
