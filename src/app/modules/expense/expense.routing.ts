import {Expenses} from '@app/modules/expense/expenses/expenses';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {Expense} from '@app/modules/expense/expense';

const routes = [
    {
        path: '',
        component: Expense,
        children: [
            {
                path: '',
                component: Expenses
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ExpenseRouting {}