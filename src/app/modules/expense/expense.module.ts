import {Expenses} from '@app/modules/expense/expenses/expenses';
import {CreateExpenseForm} from '@app/components/createExpenseForm/createExpenseForm';
import {ExpenseList} from '@app/components/expenseList/expenseList';
import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { SharedModule } from '@app/shared';

import {ExpenseRouting} from "@app/modules/expense/expense.routing";
import {Expense} from '@app/modules/expense/expense';

@NgModule({
    imports: [
        SharedModule,
        ExpenseRouting,
    ],
    declarations: [
        ExpenseList,
        CreateExpenseForm,
        Expenses,
        Expense,
    ]
})
export class ExpenseModule {
}