import {Component} from '@angular/core';
import {ReduxService} from '@app/services/redux.service';

@Component({
    selector: 'expense',
    templateUrl: './expense.html',
    styleUrls: ['./expense.scss']
})
export class Expense {
    constructor(private reduxService: ReduxService) {

    }
}