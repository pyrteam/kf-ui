import {Component, Output, EventEmitter} from '@angular/core';
import {ReduxService} from '@app/services/redux.service';
import * as ActivityActions from '@app/reducers/activity.reducer';

@Component({
    selector: 'activity-list',
    templateUrl: './activityList.html',
    styleUrls: ['./activityList.scss']
})
export class ActivityList {
    activities;

    constructor(private reduxService: ReduxService) {
        this.reduxService.selectors('activity').getActivities().subscribe(data => this.activities = data);

        this.reduxService.dispatch(new ActivityActions.Fetch());
    }

    deleteActivity(activity) {
        this.reduxService.dispatch(new ActivityActions.Delete(activity));
    }
}
