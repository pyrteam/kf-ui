import {Component} from '@angular/core';
import {ReduxService} from '@app/services/redux.service';
import * as ActivityActions from '@app/reducers/activity.reducer';

@Component({
    selector: 'create',
    templateUrl: './create.html',
    styleUrls: ['./create.scss']
})
export class Create {
    activityForm;

    constructor(private reduxService: ReduxService) {
        this.reduxService.selectors('activity').getForm().subscribe(data => this.activityForm = data);
    }

    updateValueOnInput(fieldName: string, newValue: string) {
        this.reduxService.dispatch(new ActivityActions.UpdateForm({
            updateAction: 'UPDATE_VALUE',
            fieldName: fieldName,
            newValue: newValue
        }));
    }

    validateOnBlur(fieldName: string) {
        this.reduxService.dispatch(new ActivityActions.UpdateForm({
            updateAction: 'VALIDATE_FIELD',
            fieldName: fieldName
        }));
    }

    submitForm() {
        this.reduxService.dispatch(new ActivityActions.UpdateForm({
            updateAction: 'VALIDATE_FORM',
        }));

        if (!this.activityForm.isValid) {
            return;
        }

        this.reduxService.dispatch(new ActivityActions.Create(this.activityForm.values));
    }
}
