import {Overview} from '@app/modules/activity/show/overview/overview';
import {Subscriptions} from '@app/modules/activity/show/subscriptions/subscriptions';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {Show} from '@app/modules/activity/show/show';

const routes = [
    {
        path: '',
        component: Show,
        children: [
            {
                path: 'overview',
                component: Overview
            },
            {
                path: 'subscriptions',
                component: Subscriptions
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ShowRouting {}