import {Overview} from '@app/modules/activity/show/overview/overview';
import {Subscriptions} from '@app/modules/activity/show/subscriptions/subscriptions';
import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { SharedModule } from '@app/shared';

import {ShowRouting} from "@app/modules/activity/show/show.routing";
import {Show} from '@app/modules/activity/show/show';
import {CreateSubscriptionForm} from '@app/components/createSubscriptionForm/createSubscriptionForm';
import {ActivitySubscriptionList} from '@app/components/activitySubscriptionList/activitySubscriptionList';

@NgModule({
    imports: [
        SharedModule,
        ShowRouting,
    ],
    declarations: [
        Subscriptions,
        Overview,
        Show,
        CreateSubscriptionForm,
        ActivitySubscriptionList,
    ]
})
export class ShowModule {
}