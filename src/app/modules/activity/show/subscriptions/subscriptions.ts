import {Component} from '@angular/core';
import {ReduxService} from '@app/services/redux.service';

@Component({
    selector: 'subscriptions',
    templateUrl: './subscriptions.html',
    styleUrls: ['./subscriptions.scss']
})
export class Subscriptions {
    constructor(private reduxService: ReduxService) {

    }
}
