import {Component} from '@angular/core';
import {ReduxService} from '@app/services/redux.service';

@Component({
    selector: 'overview',
    templateUrl: './overview.html',
    styleUrls: ['./overview.scss']
})
export class Overview {
    activity;

    constructor(private reduxService: ReduxService) {
        this.reduxService.selectors('activity').getActivity().subscribe(data => this.activity = data);
    }
}
