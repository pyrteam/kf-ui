import {Component} from '@angular/core';
import {ReduxService} from '@app/services/redux.service';
import {ActivatedRoute} from '@angular/router';
import * as ActivityActions from '@app/reducers/activity.reducer';

@Component({
    selector: 'show',
    templateUrl: './show.html',
    styleUrls: ['./show.scss']
})
export class Show {
    activity;

    constructor(private reduxService: ReduxService,
                private route: ActivatedRoute) {
        this.reduxService.selectors('activity').getActivity().subscribe(data => this.activity = data);
    }

    ngOnInit() {
        this.reduxService.dispatch(new ActivityActions.FetchActivity(this.route.snapshot.paramMap.get('activityId')));
    }
}