import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {Activity} from '@app/modules/activity/activity';
import {ActivityList} from '@app/modules/activity/activityList/activityList';
import {Create} from '@app/modules/activity/create/create';

const routes = [
    {
        path: '',
        component: Activity,
        children: [
            {
                path: '',
                component: ActivityList
            },
            {
                path: 'create',
                component: Create
            },
            {
                path: 'activity/:activityId',
                loadChildren: 'app/modules/activity/show/show.module#ShowModule'
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ActivityRouting {}