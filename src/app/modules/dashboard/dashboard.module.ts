import {RevenueGraph} from '@app/components/revenueGraph/revenueGraph';
import {DashboardGraphs} from '@app/modules/dashboard/dashboardGraphs/dashboardGraphs';
import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { SharedModule } from '@app/shared';

import {DashboardRouting} from "@app/modules/dashboard/dashboard.routing";
import {Dashboard} from '@app/modules/dashboard/dashboard';

@NgModule({
    imports: [
        SharedModule,
        DashboardRouting,
    ],
    declarations: [
        DashboardGraphs,
        RevenueGraph,
        Dashboard,
    ]
})
export class DashboardModule {
}