import {Component} from '@angular/core';
import {ReduxService} from '@app/services/redux.service';
import * as ExpenseActions from '@app/reducers/expense.reducer';

@Component({
    selector: 'dashboard-graphs',
    templateUrl: './dashboardGraphs.html',
    styleUrls: ['./dashboardGraphs.scss']
})
export class DashboardGraphs {
    constructor(private reduxService: ReduxService) {
        this.reduxService.dispatch(new ExpenseActions.Fetch());
    }
}
