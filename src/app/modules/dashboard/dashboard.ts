import {Component} from '@angular/core';
import {ReduxService} from '@app/services/redux.service';

@Component({
    selector: 'dashboard',
    templateUrl: './dashboard.html',
    styleUrls: ['./dashboard.scss']
})
export class Dashboard {
    constructor(private reduxService: ReduxService) {

    }
}