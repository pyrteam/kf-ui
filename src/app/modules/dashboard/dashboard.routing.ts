import {DashboardGraphs} from '@app/modules/dashboard/dashboardGraphs/dashboardGraphs';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {Dashboard} from '@app/modules/dashboard/dashboard';

const routes = [
    {
        path: '',
        component: Dashboard,
        children: [
            {
                path: '',
                component: DashboardGraphs
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DashboardRouting {}