import {StudentActivities} from '@app/modules/student/show/studentActivities/studentActivities';
import {StudentActivityList} from '@app/components/studentActivityList/studentActivityList';
import {AssignStudentActivity} from '@app/components/assignStudentActivity/assignStudentActivity';
import {Payments} from '@app/modules/student/show/payments/payments';
import {PaymentHistory} from '@app/components/paymentHistory/paymentHistory';
import {AcceptPaymentForm} from '@app/components/acceptPaymentForm/acceptPaymentForm';
import {Profile} from '@app/modules/student/show/profile/profile';
import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { SharedModule } from '@app/shared';

import {ShowRouting} from "@app/modules/student/show/show.routing";
import {Show} from '@app/modules/student/show/show';

@NgModule({
    imports: [
        SharedModule,
        ShowRouting,
    ],
    declarations: [
        AcceptPaymentForm,
        PaymentHistory,
        Payments,
        AssignStudentActivity,
        StudentActivityList,
        StudentActivities,
        Profile,
        Show,
    ]
})
export class ShowModule {
}