import {Component} from '@angular/core';
import {ReduxService} from '@app/services/redux.service';
import {ActivatedRoute} from '@angular/router';
import * as ActivityActions from '@app/reducers/activity.reducer';
import * as SubscriptionActions from '@app/reducers/subscription.reducer';
import * as InventoryActions from '@app/reducers/inventory.reducer';
import * as StudentActivityActions from '@app/reducers/studentActivity.reducer';

@Component({
    selector: 'payments',
    templateUrl: './payments.html',
    styleUrls: ['./payments.scss']
})
export class Payments {
    constructor(private reduxService: ReduxService,
                private route: ActivatedRoute) {
        this.reduxService.dispatch(new SubscriptionActions.Fetch());
        this.reduxService.dispatch(new ActivityActions.Fetch());
        this.reduxService.dispatch(new InventoryActions.Fetch());
    }

    ngOnInit() {
        this.reduxService.dispatch(new StudentActivityActions.FetchStudentActivities(this.route.parent.snapshot.paramMap.get('studentId')));
    }
}
