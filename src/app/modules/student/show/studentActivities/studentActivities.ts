import {Component} from '@angular/core';
import {ReduxService} from '@app/services/redux.service';
import * as ActivityActions from '@app/reducers/activity.reducer';
import * as SubscriptionActions from '@app/reducers/subscription.reducer';

@Component({
    selector: 'student-activities',
    templateUrl: './studentActivities.html',
    styleUrls: ['./studentActivities.scss']
})
export class StudentActivities {
    constructor(private reduxService: ReduxService) {
        this.reduxService.dispatch(new SubscriptionActions.Fetch());
        this.reduxService.dispatch(new ActivityActions.Fetch());
    }
}
