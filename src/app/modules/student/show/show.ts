import {Component} from '@angular/core';
import {ReduxService} from '@app/services/redux.service';
import {ActivatedRoute} from '@angular/router';
import * as StudentActions from '@app/reducers/student.reducer';

@Component({
    selector: 'show',
    templateUrl: './show.html',
    styleUrls: ['./show.scss']
})
export class Show {
    student;

    constructor(private reduxService: ReduxService,
                private route: ActivatedRoute) {
        this.reduxService.selectors('student').getStudent().subscribe(data => this.student = data);
    }

    ngOnInit() {
        this.reduxService.dispatch(new StudentActions.FetchStudent(this.route.snapshot.paramMap.get('studentId')));
    }
}