import {Payments} from '@app/modules/student/show/payments/payments';
import {StudentActivities} from '@app/modules/student/show/studentActivities/studentActivities';
import {Profile} from '@app/modules/student/show/profile/profile';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {Show} from '@app/modules/student/show/show';

const routes = [
    {
        path: '',
        component: Show,
        children: [
            {
                path: 'profile',
                component: Profile
            },
            {
                path: 'activities',
                component: StudentActivities
            },
            {
                path: 'payments',
                component: Payments
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ShowRouting {}