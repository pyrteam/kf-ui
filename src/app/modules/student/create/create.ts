import {Component} from '@angular/core';
import {ReduxService} from '@app/services/redux.service';

@Component({
    selector: 'create',
    templateUrl: './create.html',
    styleUrls: ['./create.scss']
})
export class Create {
    constructor(private reduxService: ReduxService) {

    }
}
