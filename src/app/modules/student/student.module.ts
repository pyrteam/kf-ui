import {CreateStudentForm} from '@app/components/createStudentForm/createStudentForm';
import {StudentList} from '@app/modules/student/studentList/studentList';
import {Create} from '@app/modules/student/create/create';
import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { SharedModule } from '@app/shared';

import {StudentRouting} from "@app/modules/student/student.routing";
import {Student} from '@app/modules/student/student';

@NgModule({
    imports: [
        SharedModule,
        StudentRouting,
    ],
    declarations: [
        CreateStudentForm,
        Student,
        Create,
        StudentList,
    ]
})
export class StudentModule {
}
