import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {StudentList} from '@app/modules/student/studentList/studentList';
import {Create} from '@app/modules/student/create/create';
import {Student} from '@app/modules/student/student';

const routes = [
    {
        path: '',
        component: Student,
        children: [
            {
                path: '',
                component: StudentList
            },
            {
                path: 'create',
                component: Create
            },
            {
                path: 'student/:studentId',
                loadChildren: './show/show.module#ShowModule'
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class StudentRouting {}