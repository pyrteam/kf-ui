import {Component} from '@angular/core';
import {ReduxService} from '@app/services/redux.service';
import * as StudentActions from '@app/reducers/student.reducer';

@Component({
    selector: 'student-list',
    templateUrl: './studentList.html',
    styleUrls: ['./studentList.scss']
})
export class StudentList {
    students;

    constructor(private reduxService: ReduxService) {
        this.reduxService.selectors('student').getStudents().subscribe(data => this.students = data);

        this.reduxService.dispatch(new StudentActions.Fetch());
    }

    deleteStudent(student) {
        this.reduxService.dispatch(new StudentActions.Delete(student));
    }
}
