import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {Inventory} from '@app/modules/inventory/inventory';
import {Items} from '@app/modules/inventory/items/items';

const routes = [
    {
        path: '',
        component: Inventory,
        children: [
            {
                path: '',
                component: Items
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class InventoryRouting {}