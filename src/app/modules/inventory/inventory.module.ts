import {CreateItemForm} from '@app/components/createItemForm/createItemForm';
import {ItemList} from '@app/components/itemList/itemList';
import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { SharedModule } from '@app/shared';

import {InventoryRouting} from "@app/modules/inventory/inventory.routing";
import {Inventory} from '@app/modules/inventory/inventory';
import {Items} from '@app/modules/inventory/items/items';

@NgModule({
    imports: [
        SharedModule,
        InventoryRouting,
    ],
    declarations: [
        Inventory,
        ItemList,
        CreateItemForm,
        Items,
    ]
})
export class InventoryModule {
}