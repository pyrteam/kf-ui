import {Component} from '@angular/core';
import {ReduxService} from '@app/services/redux.service';

@Component({
    selector: 'inventory',
    templateUrl: './inventory.html',
    styleUrls: ['./inventory.scss']
})
export class Inventory {
    constructor(private reduxService: ReduxService) {

    }
}