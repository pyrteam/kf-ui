import {Component} from '@angular/core';
import {ReduxService} from '@app/services/redux.service';
import * as InventoryActions from '@app/reducers/inventory.reducer';

@Component({
    selector: 'items',
    templateUrl: './items.html',
    styleUrls: ['./items.scss']
})
export class Items {
    constructor(private reduxService: ReduxService) {
        this.reduxService.dispatch(new InventoryActions.Fetch());
    }
}
