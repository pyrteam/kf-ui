import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {NotFound} from '@app/pages/notFound/notFound';

const routes: Routes = [
    {
        path: '',
        loadChildren: './modules/dashboard/dashboard.module#DashboardModule'
    },
    {
        path: 'students',
        loadChildren: './modules/student/student.module#StudentModule'
    },
    {
        path: 'activities',
        loadChildren: './modules/activity/activity.module#ActivityModule'
    },
    {
        path: 'expenses',
        loadChildren: './modules/expense/expense.module#ExpenseModule'
    },
    {
        path: 'inventory',
        loadChildren: './modules/inventory/inventory.module#InventoryModule'
    },
    {
        path: '**',
        component: NotFound
    }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
