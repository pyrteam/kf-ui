import {Injectable} from '@angular/core';
import {Effect, Actions} from '@ngrx/effects';
import {Observable} from "rxjs";
import {AppState} from '@app/reducers';
import {Store} from "@ngrx/store";
import {switchMap, map, catchError} from 'rxjs/operators';
import {EngineService} from '@app/services/engine.service';
import {
    PaymentActionTypes,
    FetchStudentPayments,
    FetchStudentPaymentsSuccess,
    FetchStudentPaymentsFailure,
    Delete,
    DeleteSuccess,
    DeleteFailure,
    Create,
    CreateSuccess,
    CreateFailure,
} from '@app/reducers/payment.reducer';

@Injectable()
export class PaymentEffects {
    constructor(private actions$: Actions,
                private store$: Store<AppState>,
                private engineService: EngineService
    ) { }

    @Effect() fetchStudentPayments = this.actions$
        .ofType<FetchStudentPayments>(PaymentActionTypes.FETCH_STUDENT_PAYMENTS)
        .pipe(
            map(action => action.payload),
            switchMap(payload => this.engineService.fetch('payment')
                .where('studentId', '=', payload)
                .send(
                    data => new FetchStudentPaymentsSuccess(data.payment),
                    error => new FetchStudentPaymentsFailure(error)
                ))
        );

    @Effect() delete = this.actions$
        .ofType<Delete>(PaymentActionTypes.DELETE)
        .pipe(
            map(action => action.payload),
            switchMap(payload => this.engineService.delete('payment', payload)
                .send(
                    data => new DeleteSuccess(payload),
                    error => new DeleteFailure(error)
                ))
        );

    @Effect() create = this.actions$
        .ofType<Create>(PaymentActionTypes.CREATE)
        .pipe(
            map(action => action.payload),
            switchMap(payload => this.engineService.create('payment', payload)
                .send(
                    data => new CreateSuccess(data.payment),
                    error => new CreateFailure(error)
                ))
        );
}