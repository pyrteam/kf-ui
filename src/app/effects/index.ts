import {ActivityEffects} from '@app/effects/activity.effects';
import {StudentEffects} from '@app/effects/student.effects';
import {SubscriptionEffects} from '@app/effects/subscription.effects';
import {StudentActivityEffects} from '@app/effects/studentActivity.effects';
import {PaymentEffects} from '@app/effects/payment.effects';
import {ExpenseEffects} from '@app/effects/expense.effects';
import {InventoryEffects} from '@app/effects/inventory.effects';
import {DashboardEffects} from '@app/effects/dashboard.effects';

export default [
    ActivityEffects,
    StudentEffects,
    SubscriptionEffects,
    StudentActivityEffects,
    PaymentEffects,
    ExpenseEffects,
    InventoryEffects,
    DashboardEffects
];