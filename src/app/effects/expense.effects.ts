import {Injectable} from '@angular/core';
import {Effect, Actions} from '@ngrx/effects';
import {Observable} from "rxjs";
import {AppState} from '@app/reducers';
import {Store} from "@ngrx/store";
import {switchMap, map, catchError} from 'rxjs/operators';
import {EngineService} from '@app/services/engine.service';
import {
    ExpenseActionTypes,
    Create,
    CreateSuccess,
    CreateFailure,
    Fetch,
    FetchSuccess,
    FetchFailure,
    Delete,
    DeleteSuccess,
    DeleteFailure,
} from '@app/reducers/expense.reducer';

@Injectable()
export class ExpenseEffects {
    constructor(private actions$: Actions,
                private store$: Store<AppState>,
                private engineService: EngineService
    ) { }

    @Effect() create = this.actions$
        .ofType<Create>(ExpenseActionTypes.CREATE)
        .pipe(
            map(action => action.payload),
            switchMap(payload => this.engineService.create('expense', payload)
                .send(
                    data => new CreateSuccess(data.expense),
                    error => new CreateFailure(error)
                ))
        );

    @Effect() fetch = this.actions$
        .ofType<Fetch>(ExpenseActionTypes.FETCH)
        .pipe(
            map(action => action.payload),
            switchMap(payload => this.engineService.fetch('expense')
                .send(
                    data => new FetchSuccess(data.expense),
                    error => new FetchFailure(error)
                ))
        );

    @Effect() delete = this.actions$
        .ofType<Delete>(ExpenseActionTypes.DELETE)
        .pipe(
            map(action => action.payload),
            switchMap(payload => this.engineService.delete('expense', payload)
                .send(
                    data => new DeleteSuccess(payload),
                    error => new DeleteFailure(error)
                ))
        );
}