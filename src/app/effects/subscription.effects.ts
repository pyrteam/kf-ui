import {Injectable} from '@angular/core';
import {Effect, Actions} from '@ngrx/effects';
import {Observable} from "rxjs";
import {AppState} from '@app/reducers';
import {Store} from "@ngrx/store";
import {switchMap, map, catchError} from 'rxjs/operators';
import {EngineService} from '@app/services/engine.service';
import {
    SubscriptionActionTypes,
    Create,
    CreateSuccess,
    CreateFailure,
    Delete,
    DeleteSuccess,
    DeleteFailure,
    Fetch,
    FetchSuccess,
    FetchFailure,
} from '@app/reducers/subscription.reducer';

@Injectable()
export class SubscriptionEffects {
    constructor(private actions$: Actions,
                private store$: Store<AppState>,
                private engineService: EngineService
    ) { }

    @Effect() create = this.actions$
        .ofType<Create>(SubscriptionActionTypes.CREATE)
        .pipe(
            map(action => action.payload),
            switchMap(payload => this.engineService.create('subscription', payload)
                .send(
                    data => new CreateSuccess(data.subscription),
                    error => new CreateFailure(error)
                ))
        );

    @Effect() delete = this.actions$
        .ofType<Delete>(SubscriptionActionTypes.DELETE)
        .pipe(
            map(action => action.payload),
            switchMap(payload => this.engineService.delete('subscription', payload)
                .send(
                    data => new DeleteSuccess(payload),
                    error => new DeleteFailure(error)
                ))
        );

    @Effect() fetch = this.actions$
        .ofType<Fetch>(SubscriptionActionTypes.FETCH)
        .pipe(
            map(action => action.payload),
            switchMap(payload => this.engineService.fetch('subscription')
                .send(
                    data => new FetchSuccess(data.subscription),
                    error => new FetchFailure(error)
                ))
        );
}