import {Injectable} from '@angular/core';
import {Effect, Actions} from '@ngrx/effects';
import {Observable} from "rxjs";
import {AppState} from '@app/reducers';
import {Store} from "@ngrx/store";
import {switchMap, map, catchError} from 'rxjs/operators';
import {EngineService} from '@app/services/engine.service';
import {
    StudentActionTypes,
    Fetch,
    FetchSuccess,
    FetchFailure,
    Create,
    CreateSuccess,
    CreateFailure,
    Delete,
    DeleteSuccess,
    DeleteFailure,
    FetchStudent,
    FetchStudentSuccess,
    FetchStudentFailure,
} from '@app/reducers/student.reducer';

@Injectable()
export class StudentEffects {
    constructor(private actions$: Actions,
                private store$: Store<AppState>,
                private engineService: EngineService
    ) { }

    @Effect() fetch = this.actions$
        .ofType<Fetch>(StudentActionTypes.FETCH)
        .pipe(
            map(action => action.payload),
            switchMap(payload => this.engineService.fetch('student')
                .send(
                    data => new FetchSuccess(data.student),
                    error => new FetchFailure(error)
                ))
        );

    @Effect() create = this.actions$
        .ofType<Create>(StudentActionTypes.CREATE)
        .pipe(
            map(action => action.payload),
            switchMap(payload => this.engineService.create('student', payload)
                .send(
                    data => new CreateSuccess(data.student),
                    error => new CreateFailure(error)
                ))
        );

    @Effect() delete = this.actions$
        .ofType<Delete>(StudentActionTypes.DELETE)
        .pipe(
            map(action => action.payload),
            switchMap(payload => this.engineService.delete('student', payload)
                .send(
                    data => new DeleteSuccess(payload),
                    error => new DeleteFailure(error)
                ))
        );

    @Effect() fetchStudent = this.actions$
        .ofType<FetchStudent>(StudentActionTypes.FETCH_STUDENT)
        .pipe(
            map(action => action.payload),
            switchMap(payload => this.engineService.fetch('student')
                .whereId(payload)
                .send(
                    data => new FetchStudentSuccess(data.student),
                    error => new FetchStudentFailure(error)
                ))
        );
}