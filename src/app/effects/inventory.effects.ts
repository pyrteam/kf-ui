import {Injectable} from '@angular/core';
import {Effect, Actions} from '@ngrx/effects';
import {Observable} from "rxjs";
import {AppState} from '@app/reducers';
import {Store} from "@ngrx/store";
import {switchMap, map, catchError} from 'rxjs/operators';
import {EngineService} from '@app/services/engine.service';
import {
    InventoryActionTypes,
    Create,
    CreateSuccess,
    CreateFailure,
    Fetch,
    FetchSuccess,
    FetchFailure,
    Delete,
    DeleteSuccess,
    DeleteFailure,
} from '@app/reducers/inventory.reducer';

@Injectable()
export class InventoryEffects {
    constructor(private actions$: Actions,
                private store$: Store<AppState>,
                private engineService: EngineService
    ) { }

    @Effect() create = this.actions$
        .ofType<Create>(InventoryActionTypes.CREATE)
        .pipe(
            map(action => action.payload),
            switchMap(payload => this.engineService.create('inventory', payload)
                .send(
                    data => new CreateSuccess(data.inventory),
                    error => new CreateFailure(error)
                ))
        );

    @Effect() fetch = this.actions$
        .ofType<Fetch>(InventoryActionTypes.FETCH)
        .pipe(
            map(action => action.payload),
            switchMap(payload => this.engineService.fetch('inventory')
                .send(
                    data => new FetchSuccess(data.inventory),
                    error => new FetchFailure(error)
                ))
        );

    @Effect() delete = this.actions$
        .ofType<Delete>(InventoryActionTypes.DELETE)
        .pipe(
            map(action => action.payload),
            switchMap(payload => this.engineService.delete('inventory', payload)
                .send(
                    data => new DeleteSuccess(payload),
                    error => new DeleteFailure(error)
                ))
        );
}