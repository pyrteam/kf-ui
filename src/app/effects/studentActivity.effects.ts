import {Injectable} from '@angular/core';
import {Effect, Actions} from '@ngrx/effects';
import {Observable} from "rxjs";
import {AppState} from '@app/reducers';
import {Store} from "@ngrx/store";
import {switchMap, map, catchError} from 'rxjs/operators';
import {EngineService} from '@app/services/engine.service';
import {
    StudentActivityActionTypes,
    Create,
    CreateSuccess,
    CreateFailure,
    FetchStudentActivities,
    FetchStudentActivitiesSuccess,
    FetchStudentActivitiesFailure,
    Delete,
    DeleteSuccess,
    DeleteFailure,
} from '@app/reducers/studentActivity.reducer';

@Injectable()
export class StudentActivityEffects {
    constructor(private actions$: Actions,
                private store$: Store<AppState>,
                private engineService: EngineService
    ) { }

    @Effect() create = this.actions$
        .ofType<Create>(StudentActivityActionTypes.CREATE)
        .pipe(
            map(action => action.payload),
            switchMap(payload => this.engineService.create('studentActivity', payload)
                .send(
                    data => new CreateSuccess(data.studentActivity),
                    error => new CreateFailure(error)
                ))
        );

    @Effect() fetchStudentActivities = this.actions$
        .ofType<FetchStudentActivities>(StudentActivityActionTypes.FETCH_STUDENT_ACTIVITIES)
        .pipe(
            map(action => action.payload),
            switchMap(payload => this.engineService.fetch('studentActivity')
                .where('studentId', '=', payload)
                .send(
                    data => new FetchStudentActivitiesSuccess(data.studentActivity),
                    error => new FetchStudentActivitiesFailure(error)
                ))
        );

    @Effect() delete = this.actions$
        .ofType<Delete>(StudentActivityActionTypes.DELETE)
        .pipe(
            map(action => action.payload),
            switchMap(payload => this.engineService.delete('studentActivity', payload)
                .send(
                    data => new DeleteSuccess(payload),
                    error => new DeleteFailure(error)
                ))
        );
}