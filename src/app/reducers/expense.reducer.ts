import {Form, FormField, FormFieldValidation} from '@app/utils/form';
import {List} from '@app/utils/immutable';
import {Action} from '@ngrx/store';

export interface ExpenseState {
    form: any;
    expenses: any;
}

export const initialState = {
    form: new Form({
        name: new FormField('name', '', [
            new FormFieldValidation('required', 'Name is required.')
        ]),
        amount: new FormField('amount', '', [
            new FormFieldValidation('required', 'Amount is required.')
        ]),
        date: new FormField('date', '', [
            new FormFieldValidation('required', 'Payment date is required.')
        ]),
        category: new FormField('category', ''),
        note: new FormField('note', ''),
    }),
    expenses: new List(),
};

export enum ExpenseActionTypes {
    UPDATE_FORM = 'EXPENSE_UPDATE_FORM',
    UPDATE_FORM_SUCCESS = 'EXPENSE_UPDATE_FORM_SUCCESS',
    UPDATE_FORM_FAILURE = 'EXPENSE_UPDATE_FORM_FAILURE',
    CREATE = 'EXPENSE_CREATE',
    CREATE_SUCCESS = 'EXPENSE_CREATE_SUCCESS',
    CREATE_FAILURE = 'EXPENSE_CREATE_FAILURE',
    FETCH = 'EXPENSE_FETCH',
    FETCH_SUCCESS = 'EXPENSE_FETCH_SUCCESS',
    FETCH_FAILURE = 'EXPENSE_FETCH_FAILURE',
    DELETE = 'EXPENSE_DELETE',
    DELETE_SUCCESS = 'EXPENSE_DELETE_SUCCESS',
    DELETE_FAILURE = 'EXPENSE_DELETE_FAILURE',
}

export class UpdateForm implements Action {
    readonly type = ExpenseActionTypes.UPDATE_FORM;
    constructor(public payload: any = {}) {}
}

export class UpdateFormSuccess implements Action {
    readonly type = ExpenseActionTypes.UPDATE_FORM_SUCCESS;
    constructor(public payload: any = {}) {}
}

export class UpdateFormFailure implements Action {
    readonly type = ExpenseActionTypes.UPDATE_FORM_FAILURE;
    constructor(public payload: any = {}) {}
}

export class Create implements Action {
    readonly type = ExpenseActionTypes.CREATE;
    constructor(public payload: any = {}) {}
}

export class CreateSuccess implements Action {
    readonly type = ExpenseActionTypes.CREATE_SUCCESS;
    constructor(public payload: any = {}) {}
}

export class CreateFailure implements Action {
    readonly type = ExpenseActionTypes.CREATE_FAILURE;
    constructor(public payload: any = {}) {}
}

export class Fetch implements Action {
    readonly type = ExpenseActionTypes.FETCH;
    constructor(public payload: any = {}) {}
}

export class FetchSuccess implements Action {
    readonly type = ExpenseActionTypes.FETCH_SUCCESS;
    constructor(public payload: any = {}) {}
}

export class FetchFailure implements Action {
    readonly type = ExpenseActionTypes.FETCH_FAILURE;
    constructor(public payload: any = {}) {}
}

export class Delete implements Action {
    readonly type = ExpenseActionTypes.DELETE;
    constructor(public payload: any = {}) {}
}

export class DeleteSuccess implements Action {
    readonly type = ExpenseActionTypes.DELETE_SUCCESS;
    constructor(public payload: any = {}) {}
}

export class DeleteFailure implements Action {
    readonly type = ExpenseActionTypes.DELETE_FAILURE;
    constructor(public payload: any = {}) {}
}

export type ExpenseActions =
    | UpdateForm
    | UpdateFormSuccess
    | UpdateFormFailure
    | Create
    | CreateSuccess
    | CreateFailure
    | Fetch
    | FetchSuccess
    | FetchFailure
    | Delete
    | DeleteSuccess
    | DeleteFailure;

const form = (state: any, action: any, args: any = {}) => {
    switch (action.type) {
        case ExpenseActionTypes.UPDATE_FORM:
            return state.updateForm(action.payload);
        default:
            return state;
    }
};

const expenses = (state: any, action: any, args: any = {}) => {
    switch (action.type) {
        case ExpenseActionTypes.CREATE_SUCCESS:
            return state.add(action.payload);
        case ExpenseActionTypes.FETCH_SUCCESS:
            return new List(action.payload);
        case ExpenseActionTypes.DELETE_SUCCESS:
            return state.remove(action.payload);
        default:
            return state;
    }
};

export const expenseReducer = (state: ExpenseState = initialState, action: any) => {
    switch (action.type) {
        case ExpenseActionTypes.UPDATE_FORM:
            return Object.assign({}, state, {
                form: form(state.form, action),
            });
        case ExpenseActionTypes.CREATE_SUCCESS:
            return Object.assign({}, state, {
                expenses: expenses(state.expenses, action),
            });
        case ExpenseActionTypes.FETCH_SUCCESS:
            return Object.assign({}, state, {
                expenses: expenses(state.expenses, action),
            });
        case ExpenseActionTypes.DELETE_SUCCESS:
            return Object.assign({}, state, {
                expenses: expenses(state.expenses, action),
            });
        default:
            return state;
    }
};