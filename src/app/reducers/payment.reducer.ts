import {List} from '@app/utils/immutable';
import {Form, FormField, FormFieldValidation} from '@app/utils/form';
import {Action} from '@ngrx/store';

export interface PaymentState {
    studentPayments: any;
    form: any;
}

export const initialState = {
    studentPayments: new List(),
    form: new Form({
        subject: new FormField('subject', 'activity'),
        subjectIdentifier: new FormField('subjectIdentifier', ''),
        date: new FormField('date', '', [
            new FormFieldValidation('required', 'Date is required.')
        ]),
        amount: new FormField('amount', '', [
            new FormFieldValidation('required', 'Amount is required.')
        ]),
        method: new FormField('method', 'cash'),
        note: new FormField('note', '')
    }),
};

export enum PaymentActionTypes {
    UPDATE_FORM = 'PAYMENT_UPDATE_FORM',
    UPDATE_FORM_SUCCESS = 'PAYMENT_UPDATE_FORM_SUCCESS',
    UPDATE_FORM_FAILURE = 'PAYMENT_UPDATE_FORM_FAILURE',
    CREATE = 'PAYMENT_CREATE',
    CREATE_SUCCESS = 'PAYMENT_CREATE_SUCCESS',
    CREATE_FAILURE = 'PAYMENT_CREATE_FAILURE',
    DELETE = 'PAYMENT_DELETE',
    DELETE_SUCCESS = 'PAYMENT_DELETE_SUCCESS',
    DELETE_FAILURE = 'PAYMENT_DELETE_FAILURE',
    FETCH_STUDENT_PAYMENTS = 'PAYMENT_FETCH_STUDENT_PAYMENTS',
    FETCH_STUDENT_PAYMENTS_SUCCESS = 'PAYMENT_FETCH_STUDENT_PAYMENTS_SUCCESS',
    FETCH_STUDENT_PAYMENTS_FAILURE = 'PAYMENT_FETCH_STUDENT_PAYMENTS_FAILURE',
}

export class UpdateForm implements Action {
    readonly type = PaymentActionTypes.UPDATE_FORM;
    constructor(public payload: any = {}) {}
}

export class UpdateFormSuccess implements Action {
    readonly type = PaymentActionTypes.UPDATE_FORM_SUCCESS;
    constructor(public payload: any = {}) {}
}

export class UpdateFormFailure implements Action {
    readonly type = PaymentActionTypes.UPDATE_FORM_FAILURE;
    constructor(public payload: any = {}) {}
}

export class Create implements Action {
    readonly type = PaymentActionTypes.CREATE;
    constructor(public payload: any = {}) {}
}

export class CreateSuccess implements Action {
    readonly type = PaymentActionTypes.CREATE_SUCCESS;
    constructor(public payload: any = {}) {}
}

export class CreateFailure implements Action {
    readonly type = PaymentActionTypes.CREATE_FAILURE;
    constructor(public payload: any = {}) {}
}

export class Delete implements Action {
    readonly type = PaymentActionTypes.DELETE;
    constructor(public payload: any = {}) {}
}

export class DeleteSuccess implements Action {
    readonly type = PaymentActionTypes.DELETE_SUCCESS;
    constructor(public payload: any = {}) {}
}

export class DeleteFailure implements Action {
    readonly type = PaymentActionTypes.DELETE_FAILURE;
    constructor(public payload: any = {}) {}
}

export class FetchStudentPayments implements Action {
    readonly type = PaymentActionTypes.FETCH_STUDENT_PAYMENTS;
    constructor(public payload: any = {}) {}
}

export class FetchStudentPaymentsSuccess implements Action {
    readonly type = PaymentActionTypes.FETCH_STUDENT_PAYMENTS_SUCCESS;
    constructor(public payload: any = {}) {}
}

export class FetchStudentPaymentsFailure implements Action {
    readonly type = PaymentActionTypes.FETCH_STUDENT_PAYMENTS_FAILURE;
    constructor(public payload: any = {}) {}
}

export type PaymentActions =
    | UpdateForm
    | UpdateFormSuccess
    | UpdateFormFailure
    | Create
    | CreateSuccess
    | CreateFailure
    | Delete
    | DeleteSuccess
    | DeleteFailure
    | FetchStudentPayments
    | FetchStudentPaymentsSuccess
    | FetchStudentPaymentsFailure;

const studentPayments = (state: any, action: any, args: any = {}) => {
    switch (action.type) {
        case PaymentActionTypes.FETCH_STUDENT_PAYMENTS_SUCCESS:
            return new List(action.payload);
        case PaymentActionTypes.DELETE_SUCCESS:
            return state.remove(action.payload);
        case PaymentActionTypes.CREATE_SUCCESS:
            return state.add(action.payload);
        default:
            return state;
    }
};

const form = (state: any, action: any, args: any = {}) => {
    switch (action.type) {
        case PaymentActionTypes.UPDATE_FORM:
            return state.updateForm(action.payload);
        default:
            return state;
    }
};

export const paymentReducer = (state: PaymentState = initialState, action: any) => {
    switch (action.type) {
        case PaymentActionTypes.UPDATE_FORM:
            return Object.assign({}, state, {
                form: form(state.form, action),
            });
        case PaymentActionTypes.FETCH_STUDENT_PAYMENTS_SUCCESS:
            return Object.assign({}, state, {
                studentPayments: studentPayments(state.studentPayments, action),
            });
        case PaymentActionTypes.DELETE_SUCCESS:
            return Object.assign({}, state, {
                studentPayments: studentPayments(state.studentPayments, action),
            });
        case PaymentActionTypes.CREATE_SUCCESS:
            return Object.assign({}, state, {
                studentPayments: studentPayments(state.studentPayments, action),
            });
        default:
            return state;
    }
};