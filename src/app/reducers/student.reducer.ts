import {Action} from '@ngrx/store';
import {List, Entity} from '@app/utils/immutable';
import {Form, FormField, FormFieldValidation} from '@app/utils/form';

export interface StudentState {
    students: any;
    form: any;
    student: any;
}

export const initialState = {
    students: new List(),
    form: new Form({
        firstName: new FormField('firstName', '', [
            new FormFieldValidation('required', 'First name is required.')
        ]),
        lastName: new FormField('lastName', '', [
            new FormFieldValidation('required', 'Last name is required.')
        ])
    }),
    student: new Entity(),
};

export enum StudentActionTypes {
    FETCH = 'STUDENT_FETCH',
    FETCH_SUCCESS = 'STUDENT_FETCH_SUCCESS',
    FETCH_FAILURE = 'STUDENT_FETCH_FAILURE',
    UPDATE_FORM = 'STUDENT_UPDATE_FORM',
    UPDATE_FORM_SUCCESS = 'STUDENT_UPDATE_FORM_SUCCESS',
    UPDATE_FORM_FAILURE = 'STUDENT_UPDATE_FORM_FAILURE',
    CREATE = 'STUDENT_CREATE',
    CREATE_SUCCESS = 'STUDENT_CREATE_SUCCESS',
    CREATE_FAILURE = 'STUDENT_CREATE_FAILURE',
    DELETE = 'STUDENT_DELETE',
    DELETE_SUCCESS = 'STUDENT_DELETE_SUCCESS',
    DELETE_FAILURE = 'STUDENT_DELETE_FAILURE',
    FETCH_STUDENT = 'STUDENT_FETCH_STUDENT',
    FETCH_STUDENT_SUCCESS = 'STUDENT_FETCH_STUDENT_SUCCESS',
    FETCH_STUDENT_FAILURE = 'STUDENT_FETCH_STUDENT_FAILURE',
}

export class Fetch implements Action {
    readonly type = StudentActionTypes.FETCH;
    constructor(public payload: any = {}) {}
}

export class FetchSuccess implements Action {
    readonly type = StudentActionTypes.FETCH_SUCCESS;
    constructor(public payload: any = {}) {}
}

export class FetchFailure implements Action {
    readonly type = StudentActionTypes.FETCH_FAILURE;
    constructor(public payload: any = {}) {}
}

export class UpdateForm implements Action {
    readonly type = StudentActionTypes.UPDATE_FORM;
    constructor(public payload: any = {}) {}
}

export class UpdateFormSuccess implements Action {
    readonly type = StudentActionTypes.UPDATE_FORM_SUCCESS;
    constructor(public payload: any = {}) {}
}

export class UpdateFormFailure implements Action {
    readonly type = StudentActionTypes.UPDATE_FORM_FAILURE;
    constructor(public payload: any = {}) {}
}

export class Create implements Action {
    readonly type = StudentActionTypes.CREATE;
    constructor(public payload: any = {}) {}
}

export class CreateSuccess implements Action {
    readonly type = StudentActionTypes.CREATE_SUCCESS;
    constructor(public payload: any = {}) {}
}

export class CreateFailure implements Action {
    readonly type = StudentActionTypes.CREATE_FAILURE;
    constructor(public payload: any = {}) {}
}

export class Delete implements Action {
    readonly type = StudentActionTypes.DELETE;
    constructor(public payload: any = {}) {}
}

export class DeleteSuccess implements Action {
    readonly type = StudentActionTypes.DELETE_SUCCESS;
    constructor(public payload: any = {}) {}
}

export class DeleteFailure implements Action {
    readonly type = StudentActionTypes.DELETE_FAILURE;
    constructor(public payload: any = {}) {}
}

export class FetchStudent implements Action {
    readonly type = StudentActionTypes.FETCH_STUDENT;
    constructor(public payload: any = {}) {}
}

export class FetchStudentSuccess implements Action {
    readonly type = StudentActionTypes.FETCH_STUDENT_SUCCESS;
    constructor(public payload: any = {}) {}
}

export class FetchStudentFailure implements Action {
    readonly type = StudentActionTypes.FETCH_STUDENT_FAILURE;
    constructor(public payload: any = {}) {}
}

export type StudentActions =
    | Fetch
    | FetchSuccess
    | FetchFailure
    | UpdateForm
    | UpdateFormSuccess
    | UpdateFormFailure
    | Create
    | CreateSuccess
    | CreateFailure
    | Delete
    | DeleteSuccess
    | DeleteFailure
    | FetchStudent
    | FetchStudentSuccess
    | FetchStudentFailure;

const students = (state: any, action: any, args: any = {}) => {
    switch (action.type) {
        case StudentActionTypes.FETCH_SUCCESS:
            return new List(action.payload);
        case StudentActionTypes.CREATE_SUCCESS:
            return state.add(action.payload);
        case StudentActionTypes.DELETE_SUCCESS:
            return state.remove(action.payload);
        default:
            return state;
    }
};

const form = (state: any, action: any, args: any = {}) => {
    switch (action.type) {
        case StudentActionTypes.UPDATE_FORM:
            return state.updateForm(action.payload);
        default:
            return state;
    }
};

const student = (state: any, action: any, args: any = {}) => {
    switch (action.type) {
        case StudentActionTypes.FETCH_STUDENT_SUCCESS:
            return new Entity(action.payload);
        default:
            return state;
    }
};

export const studentReducer = (state: StudentState = initialState, action: any) => {
    switch (action.type) {
        case StudentActionTypes.FETCH_SUCCESS:
            return Object.assign({}, state, {
                students: students(state.students, action),
            });
        case StudentActionTypes.UPDATE_FORM:
            return Object.assign({}, state, {
                form: form(state.form, action),
            });
        case StudentActionTypes.CREATE_SUCCESS:
            return Object.assign({}, state, {
                students: students(state.students, action),
            });
        case StudentActionTypes.DELETE_SUCCESS:
            return Object.assign({}, state, {
                students: students(state.students, action),
            });
        case StudentActionTypes.FETCH_STUDENT_SUCCESS:
            return Object.assign({}, state, {
                student: student(state.student, action),
            });
        default:
            return state;
    }
};