import {List} from '@app/utils/immutable';
import {Form, FormField, FormFieldValidation} from '@app/utils/form';
import {Action} from '@ngrx/store';

export interface InventoryState {
    items: any;
    form: any;
}

export const initialState = {
    items: new List(),
    form: new Form({
        name: new FormField('name', '', [
            new FormFieldValidation('required', 'Name is required.')
        ]),
        price: new FormField('price', '', [
            new FormFieldValidation('required', 'Price is required.')
        ]),
        isForSale: new FormField('isForSale', true),
        quantity: new FormField('quantity', '', [
            new FormFieldValidation('required', 'Quantity is required.')
        ]),
        category: new FormField('category', '')
    }),
};

export enum InventoryActionTypes {
    UPDATE_FORM = 'INVENTORY_UPDATE_FORM',
    UPDATE_FORM_SUCCESS = 'INVENTORY_UPDATE_FORM_SUCCESS',
    UPDATE_FORM_FAILURE = 'INVENTORY_UPDATE_FORM_FAILURE',
    CREATE = 'INVENTORY_CREATE',
    CREATE_SUCCESS = 'INVENTORY_CREATE_SUCCESS',
    CREATE_FAILURE = 'INVENTORY_CREATE_FAILURE',
    FETCH = 'INVENTORY_FETCH',
    FETCH_SUCCESS = 'INVENTORY_FETCH_SUCCESS',
    FETCH_FAILURE = 'INVENTORY_FETCH_FAILURE',
    DELETE = 'INVENTORY_DELETE',
    DELETE_SUCCESS = 'INVENTORY_DELETE_SUCCESS',
    DELETE_FAILURE = 'INVENTORY_DELETE_FAILURE',
}

export class UpdateForm implements Action {
    readonly type = InventoryActionTypes.UPDATE_FORM;
    constructor(public payload: any = {}) {}
}

export class UpdateFormSuccess implements Action {
    readonly type = InventoryActionTypes.UPDATE_FORM_SUCCESS;
    constructor(public payload: any = {}) {}
}

export class UpdateFormFailure implements Action {
    readonly type = InventoryActionTypes.UPDATE_FORM_FAILURE;
    constructor(public payload: any = {}) {}
}

export class Create implements Action {
    readonly type = InventoryActionTypes.CREATE;
    constructor(public payload: any = {}) {}
}

export class CreateSuccess implements Action {
    readonly type = InventoryActionTypes.CREATE_SUCCESS;
    constructor(public payload: any = {}) {}
}

export class CreateFailure implements Action {
    readonly type = InventoryActionTypes.CREATE_FAILURE;
    constructor(public payload: any = {}) {}
}

export class Fetch implements Action {
    readonly type = InventoryActionTypes.FETCH;
    constructor(public payload: any = {}) {}
}

export class FetchSuccess implements Action {
    readonly type = InventoryActionTypes.FETCH_SUCCESS;
    constructor(public payload: any = {}) {}
}

export class FetchFailure implements Action {
    readonly type = InventoryActionTypes.FETCH_FAILURE;
    constructor(public payload: any = {}) {}
}

export class Delete implements Action {
    readonly type = InventoryActionTypes.DELETE;
    constructor(public payload: any = {}) {}
}

export class DeleteSuccess implements Action {
    readonly type = InventoryActionTypes.DELETE_SUCCESS;
    constructor(public payload: any = {}) {}
}

export class DeleteFailure implements Action {
    readonly type = InventoryActionTypes.DELETE_FAILURE;
    constructor(public payload: any = {}) {}
}

export type InventoryActions =
    | UpdateForm
    | UpdateFormSuccess
    | UpdateFormFailure
    | Create
    | CreateSuccess
    | CreateFailure
    | Fetch
    | FetchSuccess
    | FetchFailure
    | Delete
    | DeleteSuccess
    | DeleteFailure;

const items = (state: any, action: any, args: any = {}) => {
    switch (action.type) {
        case InventoryActionTypes.CREATE_SUCCESS:
            return state.add(action.payload);
        case InventoryActionTypes.FETCH_SUCCESS:
            return new List(action.payload);
        case InventoryActionTypes.DELETE_SUCCESS:
            return state.remove(action.payload);
        default:
            return state;
    }
};

const form = (state: any, action: any, args: any = {}) => {
    switch (action.type) {
        case InventoryActionTypes.UPDATE_FORM:
            return state.updateForm(action.payload);
        default:
            return state;
    }
};

export const inventoryReducer = (state: InventoryState = initialState, action: any) => {
    switch (action.type) {
        case InventoryActionTypes.UPDATE_FORM:
            return Object.assign({}, state, {
                form: form(state.form, action),
            });
        case InventoryActionTypes.CREATE_SUCCESS:
            return Object.assign({}, state, {
                items: items(state.items, action),
            });
        case InventoryActionTypes.FETCH_SUCCESS:
            return Object.assign({}, state, {
                items: items(state.items, action),
            });
        case InventoryActionTypes.DELETE_SUCCESS:
            return Object.assign({}, state, {
                items: items(state.items, action),
            });
        default:
            return state;
    }
};