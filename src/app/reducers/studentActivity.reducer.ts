import {Form, FormField, FormFieldValidation} from '@app/utils/form';
import {List} from '@app/utils/immutable';
import {Action} from '@ngrx/store';

export interface StudentActivityState {
    form: any;
    studentActivities: any;
    showForm: boolean;
}

export const initialState = {
    form: new Form({
        activityId: new FormField('activityId', ''),
        subscriptionId: new FormField('subscriptionId', ''),
        startDate: new FormField('startDate', '', [
            new FormFieldValidation('required', 'Starting date is required.')
        ]),
    }),
    studentActivities: new List(),
    showForm: true,
};

export enum StudentActivityActionTypes {
    UPDATE_FORM = 'STUDENT_ACTIVITY_UPDATE_FORM',
    UPDATE_FORM_SUCCESS = 'STUDENT_ACTIVITY_UPDATE_FORM_SUCCESS',
    UPDATE_FORM_FAILURE = 'STUDENT_ACTIVITY_UPDATE_FORM_FAILURE',
    CREATE = 'STUDENT_ACTIVITY_CREATE',
    CREATE_SUCCESS = 'STUDENT_ACTIVITY_CREATE_SUCCESS',
    CREATE_FAILURE = 'STUDENT_ACTIVITY_CREATE_FAILURE',
    FETCH_STUDENT_ACTIVITIES = 'STUDENT_ACTIVITY_FETCH_STUDENT_ACTIVITIES',
    FETCH_STUDENT_ACTIVITIES_SUCCESS = 'STUDENT_ACTIVITY_FETCH_STUDENT_ACTIVITIES_SUCCESS',
    FETCH_STUDENT_ACTIVITIES_FAILURE = 'STUDENT_ACTIVITY_FETCH_STUDENT_ACTIVITIES_FAILURE',
    DELETE = 'STUDENT_ACTIVITY_DELETE',
    DELETE_SUCCESS = 'STUDENT_ACTIVITY_DELETE_SUCCESS',
    DELETE_FAILURE = 'STUDENT_ACTIVITY_DELETE_FAILURE',
}

export class UpdateForm implements Action {
    readonly type = StudentActivityActionTypes.UPDATE_FORM;
    constructor(public payload: any = {}) {}
}

export class UpdateFormSuccess implements Action {
    readonly type = StudentActivityActionTypes.UPDATE_FORM_SUCCESS;
    constructor(public payload: any = {}) {}
}

export class UpdateFormFailure implements Action {
    readonly type = StudentActivityActionTypes.UPDATE_FORM_FAILURE;
    constructor(public payload: any = {}) {}
}

export class Create implements Action {
    readonly type = StudentActivityActionTypes.CREATE;
    constructor(public payload: any = {}) {}
}

export class CreateSuccess implements Action {
    readonly type = StudentActivityActionTypes.CREATE_SUCCESS;
    constructor(public payload: any = {}) {}
}

export class CreateFailure implements Action {
    readonly type = StudentActivityActionTypes.CREATE_FAILURE;
    constructor(public payload: any = {}) {}
}

export class FetchStudentActivities implements Action {
    readonly type = StudentActivityActionTypes.FETCH_STUDENT_ACTIVITIES;
    constructor(public payload: any = {}) {}
}

export class FetchStudentActivitiesSuccess implements Action {
    readonly type = StudentActivityActionTypes.FETCH_STUDENT_ACTIVITIES_SUCCESS;
    constructor(public payload: any = {}) {}
}

export class FetchStudentActivitiesFailure implements Action {
    readonly type = StudentActivityActionTypes.FETCH_STUDENT_ACTIVITIES_FAILURE;
    constructor(public payload: any = {}) {}
}

export class Delete implements Action {
    readonly type = StudentActivityActionTypes.DELETE;
    constructor(public payload: any = {}) {}
}

export class DeleteSuccess implements Action {
    readonly type = StudentActivityActionTypes.DELETE_SUCCESS;
    constructor(public payload: any = {}) {}
}

export class DeleteFailure implements Action {
    readonly type = StudentActivityActionTypes.DELETE_FAILURE;
    constructor(public payload: any = {}) {}
}

export type StudentActivityActions =
    | UpdateForm
    | UpdateFormSuccess
    | UpdateFormFailure
    | Create
    | CreateSuccess
    | CreateFailure
    | FetchStudentActivities
    | FetchStudentActivitiesSuccess
    | FetchStudentActivitiesFailure
    | Delete
    | DeleteSuccess
    | DeleteFailure;

const form = (state: any, action: any, args: any = {}) => {
    switch (action.type) {
        case StudentActivityActionTypes.UPDATE_FORM:
            return state.updateForm(action.payload);
        default:
            return state;
    }
};

const studentActivities = (state: any, action: any, args: any = {}) => {
    switch (action.type) {
        case StudentActivityActionTypes.CREATE_SUCCESS:
            return state.add(action.payload);
        case StudentActivityActionTypes.FETCH_STUDENT_ACTIVITIES_SUCCESS:
            return new List(action.payload);
        case StudentActivityActionTypes.DELETE_SUCCESS:
            return state.remove(action.payload);
        default:
            return state;
    }
};

const showForm = (state: any, action: any, args: any = {}) => {
    switch (action.type) {
        default:
            return state;
    }
};

export const studentActivityReducer = (state: StudentActivityState = initialState, action: any) => {
    switch (action.type) {
        case StudentActivityActionTypes.UPDATE_FORM:
            return Object.assign({}, state, {
                form: form(state.form, action),
            });
        case StudentActivityActionTypes.CREATE_SUCCESS:
            return Object.assign({}, state, {
                studentActivities: studentActivities(state.studentActivities, action),
            });
        case StudentActivityActionTypes.FETCH_STUDENT_ACTIVITIES_SUCCESS:
            return Object.assign({}, state, {
                studentActivities: studentActivities(state.studentActivities, action),
            });
        case StudentActivityActionTypes.DELETE_SUCCESS:
            return Object.assign({}, state, {
                studentActivities: studentActivities(state.studentActivities, action),
            });
        default:
            return state;
    }
};