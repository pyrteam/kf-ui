import {Action} from '@ngrx/store';

export interface DashboardState {
}

export const initialState = {
};

export enum DashboardActionTypes {
    TEST = 'DASHBOARD_TEST',
}

export class Test implements Action {
    readonly type = DashboardActionTypes.TEST;
    constructor(public payload: any) {}
}

export type DashboardActions =
    | Test;

export const dashboardReducer = (state: DashboardState = initialState, action: any) => {
    switch (action.type) {
        default:
            return state;
    }
};