import {List} from '@app/utils/immutable';
import {Form, FormField, FormFieldValidation} from '@app/utils/form';
import {Action} from '@ngrx/store';

export interface SubscriptionState {
    subscriptions: any;
    form: any;
}

export const initialState = {
    subscriptions: new List(),
    form: new Form({
        rate: new FormField('rate', '', [
            new FormFieldValidation('required', 'Rate is required.')
        ]),
        period: new FormField('period', 'monthly')
    }),
};

export enum SubscriptionActionTypes {
    UPDATE_FORM = 'SUBSCRIPTION_UPDATE_FORM',
    UPDATE_FORM_SUCCESS = 'SUBSCRIPTION_UPDATE_FORM_SUCCESS',
    UPDATE_FORM_FAILURE = 'SUBSCRIPTION_UPDATE_FORM_FAILURE',
    CREATE = 'SUBSCRIPTION_CREATE',
    CREATE_SUCCESS = 'SUBSCRIPTION_CREATE_SUCCESS',
    CREATE_FAILURE = 'SUBSCRIPTION_CREATE_FAILURE',
    DELETE = 'SUBSCRIPTION_DELETE',
    DELETE_SUCCESS = 'SUBSCRIPTION_DELETE_SUCCESS',
    DELETE_FAILURE = 'SUBSCRIPTION_DELETE_FAILURE',
    FETCH = 'SUBSCRIPTION_FETCH',
    FETCH_SUCCESS = 'SUBSCRIPTION_FETCH_SUCCESS',
    FETCH_FAILURE = 'SUBSCRIPTION_FETCH_FAILURE',
}

export class UpdateForm implements Action {
    readonly type = SubscriptionActionTypes.UPDATE_FORM;
    constructor(public payload: any = {}) {}
}

export class UpdateFormSuccess implements Action {
    readonly type = SubscriptionActionTypes.UPDATE_FORM_SUCCESS;
    constructor(public payload: any = {}) {}
}

export class UpdateFormFailure implements Action {
    readonly type = SubscriptionActionTypes.UPDATE_FORM_FAILURE;
    constructor(public payload: any = {}) {}
}

export class Create implements Action {
    readonly type = SubscriptionActionTypes.CREATE;
    constructor(public payload: any = {}) {}
}

export class CreateSuccess implements Action {
    readonly type = SubscriptionActionTypes.CREATE_SUCCESS;
    constructor(public payload: any = {}) {}
}

export class CreateFailure implements Action {
    readonly type = SubscriptionActionTypes.CREATE_FAILURE;
    constructor(public payload: any = {}) {}
}

export class Delete implements Action {
    readonly type = SubscriptionActionTypes.DELETE;
    constructor(public payload: any = {}) {}
}

export class DeleteSuccess implements Action {
    readonly type = SubscriptionActionTypes.DELETE_SUCCESS;
    constructor(public payload: any = {}) {}
}

export class DeleteFailure implements Action {
    readonly type = SubscriptionActionTypes.DELETE_FAILURE;
    constructor(public payload: any = {}) {}
}

export class Fetch implements Action {
    readonly type = SubscriptionActionTypes.FETCH;
    constructor(public payload: any = {}) {}
}

export class FetchSuccess implements Action {
    readonly type = SubscriptionActionTypes.FETCH_SUCCESS;
    constructor(public payload: any = {}) {}
}

export class FetchFailure implements Action {
    readonly type = SubscriptionActionTypes.FETCH_FAILURE;
    constructor(public payload: any = {}) {}
}

export type SubscriptionActions =
    | UpdateForm
    | UpdateFormSuccess
    | UpdateFormFailure
    | Create
    | CreateSuccess
    | CreateFailure
    | Delete
    | DeleteSuccess
    | DeleteFailure
    | Fetch
    | FetchSuccess
    | FetchFailure;

const subscriptions = (state: any, action: any, args: any = {}) => {
    switch (action.type) {
        case SubscriptionActionTypes.CREATE_SUCCESS:
            return state.add(action.payload);
        case SubscriptionActionTypes.DELETE_SUCCESS:
            return state.remove(action.payload);
        case SubscriptionActionTypes.FETCH_SUCCESS:
            return new List(action.payload);
        default:
            return state;
    }
};

const form = (state: any, action: any, args: any = {}) => {
    switch (action.type) {
        case SubscriptionActionTypes.UPDATE_FORM:
            return state.updateForm(action.payload);
        default:
            return state;
    }
};

export const subscriptionReducer = (state: SubscriptionState = initialState, action: any) => {
    switch (action.type) {
        case SubscriptionActionTypes.UPDATE_FORM:
            return Object.assign({}, state, {
                form: form(state.form, action),
            });
        case SubscriptionActionTypes.CREATE_SUCCESS:
            return Object.assign({}, state, {
                subscriptions: subscriptions(state.subscriptions, action),
            });
        case SubscriptionActionTypes.DELETE_SUCCESS:
            return Object.assign({}, state, {
                subscriptions: subscriptions(state.subscriptions, action),
            });
        case SubscriptionActionTypes.FETCH_SUCCESS:
            return Object.assign({}, state, {
                subscriptions: subscriptions(state.subscriptions, action),
            });
        default:
            return state;
    }
};