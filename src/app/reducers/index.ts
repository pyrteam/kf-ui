import {ActionReducerMap} from '@ngrx/store';
import {InjectionToken} from '@angular/core';
import {ActivityState, activityReducer} from '@app/reducers/activity.reducer';
import {StudentState, studentReducer} from '@app/reducers/student.reducer';
import {SubscriptionState, subscriptionReducer} from '@app/reducers/subscription.reducer';
import {StudentActivityState, studentActivityReducer} from '@app/reducers/studentActivity.reducer';
import {PaymentState, paymentReducer} from '@app/reducers/payment.reducer';
import {ExpenseState, expenseReducer} from '@app/reducers/expense.reducer';
import {InventoryState, inventoryReducer} from '@app/reducers/inventory.reducer';
import {DashboardState, dashboardReducer} from '@app/reducers/dashboard.reducer';

export interface AppState {
    activity: ActivityState,
    student: StudentState,
    subscription: SubscriptionState,
    studentActivity: StudentActivityState,
    payment: PaymentState,
    expense: ExpenseState,
    inventory: InventoryState,
    dashboard: DashboardState
}

export const reducerToken = new InjectionToken<ActionReducerMap<AppState>>('Reducers');

export function getReducers() {
    return {
        activity: activityReducer,
        student: studentReducer,
        subscription: subscriptionReducer,
        studentActivity: studentActivityReducer,
        payment: paymentReducer,
        expense: expenseReducer,
        inventory: inventoryReducer,
        dashboard: dashboardReducer
    };
}

export const reducerProvider = [
    { provide: reducerToken, useFactory: getReducers }
];